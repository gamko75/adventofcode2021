def getMeasurements():
    with open("./input.txt") as fp:
        lines = fp.readlines()
        lastValue = None
        measurementCount = 0
        for line in lines:
            if lastValue is not None:
                if int(line) > int(lastValue):
                    measurementCount += 1  
            lastValue = line         
        return measurementCount    

def getMeasurementsPart2():
    increases = 0
    arr = []
    with open("./input.txt") as fp:
        for i in fp.readlines():
            arr.append(int(i.rstrip("\n")))


    for i in range(len(arr)):

        if sum(arr[i:i+3]) < sum(arr[i+1:i+4]):
            increases+=1

    return increases

    
if __name__ == "__main__":
    print(getMeasurements(), getMeasurementsPart2())